import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PinDuoDuo';
  language: 'en' | 'am' | 'ru' = 'en';
  constructor() {
    let date = new Date().toString();
    if (date.includes('Armenia')) {
      this.language = 'am'
    } else if (date.includes('Russia')) {
      this.language = 'ru'
    } else {
      this.language = 'en'
    }
  }
}
