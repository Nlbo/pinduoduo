const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const {v4: uuidv4} = require('uuid');
const Process = require("process");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(require('cors')());
app.use(express.static(__dirname + '/dist/pin-duo-duo'));
app.use(express.static(__dirname + '/dist/pin-duo-duo/assets'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/pin-duo-duo/index.html'));
});
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/pin-duo-duo/index.html'));
});

app.use((req, res, next) => {
  const err = new Error('Not found');
  err.status = 404;
  res.status(err.status).json({error: err.message})
});
app.use((err, req, res, next) => {
  console.error(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).json({error: err.message});
});

app.listen(Process.env.PORT || 3000, () => console.log(`Server has started on ${Process.env.PORT || 3000} port`) )
